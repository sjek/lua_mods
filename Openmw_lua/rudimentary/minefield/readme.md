this mod creates mushrooms around player that "detonate" and have vfx effect + diminish the health when player come too close 

- Works by pressing c, atm. one mine does 20 points of dmg and field is dense so kills default character quickly.

```
1. PLAYER: scripts/mines_trigger.lua
2. GLOBAL: scripts/mines_global_placement.lua
3. CUSTOM: scripts/mines_custom_vfx.lua
```

1. trigger in player script : works in this case with onKeyPress enginehandler sending the event to global for mine placement. 
    - can be better to trigger on quest stage

2. global handles the events placing and removing of mines. currently places them to grid around player. also manual center position can be used

3. custom script on mines does the distance check againts player ( probably can do better than time.runRepeatedly ) and handles the vfx. 
    - also sents the event to global to remove the "mine" and event to player to diminish health as it needs self. uses the statics from damagehealth magic effect.
    - the vfx is scaled depending on distance

