local world = require('openmw.world')
local core = require('openmw.core')
local util = require('openmw.util')

local function place_mines(func)

        local mines = world.createObject( "flora_bc_mushroom_01" ,25)
                            -- objects to place, needed number
            local coor = {} 
            for i=1, 5 do
              for j=1 , 5 do
               local mine = mines:split(1)  -- split for every spot
           
                coor = util.vector3(   -- makes the coordinates, corner and add
                      func.player.position.x - 500 + math.random(50,200)*i, 
                      func.player.position.y - 500 + math.random(50,200)*j,
                      func.player.position.z )
              
                mine:teleport( func.player.cell, coor ) -- place the objects 
                mine:addScript("scripts/mines_custom_vfx.lua")               
              end
            end
end

local function minerem(func)
  func.mineself:remove()
end

return {
    eventHandlers = { sjek_place_mines = place_mines, sjek_mine_remove = minerem }
}
