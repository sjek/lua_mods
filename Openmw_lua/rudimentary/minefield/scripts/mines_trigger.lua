local core = require('openmw.core')
local self = require('openmw.self')
local types = require('openmw.types')


local function onKeyPress(key)  
  if key.symbol == 'c' then   -- the trigger

        core.sendGlobalEvent("sjek_place_mines", { player = self} )
              -- placing event to global
  end 
end


local function diminish(var)
  types.Actor.stats.dynamic.health(self).current = types.Actor.stats.dynamic.health(self).current - 20
      -- diminish the player health. can be moved to another script to affect also npc and creatures
end

return { engineHandlers = { onKeyPress = onKeyPress },
         eventHandlers = { sjek_mine_reduction = diminish }} 
