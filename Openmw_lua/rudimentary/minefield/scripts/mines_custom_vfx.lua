local I = require('openmw.interfaces')
local types = require('openmw.types')
local time = require('openmw_aux.time')
local nearby = require('openmw.nearby')
local self = require('openmw.self')
local core = require('openmw.core')

  time.runRepeatedly(function()
    local near = nearby.players 
      for a,_ in pairs(near) do
        local di = ( near[a].position - self.position ):length() 
                        -- the distance
        
        if di < 500 and di > 100 then
          local effe1 = core.magic.effects.records["DamageHealth"].areaStatic 
                      -- the vfx 
        
          core.vfx.spawn(effe1, self.position, { scale = 500 / di} ) -- scaling
        end
        
      end
   end, 1*time.second )

  time.runRepeatedly(function()
    local near = nearby.players 
      for a,_ in pairs(near) do
        local di = ( near[a].position - self.position ):length()
                
        if di < 100 then
          near[a]:sendEvent("sjek_mine_reduction")

          local effe2 = core.magic.effects.records["DamageHealth"].castStatic
          core.vfx.spawn(effe2, self.position )
          
          core.sendGlobalEvent("sjek_mine_remove", { mineself = self } )
                  -- remove event
         
        end
      end
   end, 1*time.second )
