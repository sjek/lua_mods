local nearby = require('openmw.nearby')
local core = require('openmw.core')
local util = require('openmw.util')
local self = require('openmw.self')
local time = require('openmw_aux.time')


local function sjek_fire_direction(var)
 
  local t = 0

    local speed = var.centervect*150
    local gravity = util.vector3(0,0,-40)
 
    time.runRepeatedly(function()
   
    t = t + 0.2

    local coord = self.position + speed*t + gravity * t^2

    core.sendGlobalEvent("sjek_move_fire", { selfa = self, movedir = coord })

      local hits = nearby.castRay(self.position, coord, {
            ignore = { 
                var.player, 
                self, 
                var.trailobject 
             } })
      
      core.sendGlobalEvent("sjek_summontrail",{ pos = self })
      
      if hits.hit == true then 
         print(tostring(hits.hitObject))
         core.sendGlobalEvent("sjek_hitposremove",{ hits = hits.hitPos, selfs = self})
      end
      
  end,0.2*time.second)
  
end

return {
    eventHandlers = { sjek_fire_direction = sjek_fire_direction }
}
