local core = require('openmw.core')
local world = require('openmw.world')
local util = require('openmw.util')
local time = require('openmw_aux.time')

local endobject = "crate_02_pillows"
local trailobject = "apparatus_m_alembic_01"
local flightobject = "Light_Fire_NoSmoke"


local function sjek_throw_fireball(so)
  
  local fireball = world.createObject( flightobject, 1)
        fireball:addScript("fireball/fireball_local.lua")
  
  local start = so.player.position + so.centervect*100 + util.vector3(0,0,50)
  fireball:teleport( so.player.cell, start )

  fireball:sendEvent("sjek_fire_direction",{ player = so.player , centervect = so.centervect, trailobject = trailobject })  
end

local function sjek_move_fire(mover)
    mover.selfa:teleport( mover.selfa.cell, mover.movedir )
end

local function sjek_hitposremove(faar)
  world.createObject( endobject, 1):teleport( faar.selfs.cell , faar.hits )
  faar.selfs.enabled = false
end

local function sjek_summontrail(vari)
    world.createObject( trailobject ,1):teleport(vari.pos.cell,vari.pos.position)
end


return {
    eventHandlers = { 
            sjek_throw_fireball = sjek_throw_fireball, 
            sjek_hitposremove = sjek_hitposremove, 
            sjek_move_fire = sjek_move_fire, 
            sjek_summontrail = sjek_summontrail 
            }}
