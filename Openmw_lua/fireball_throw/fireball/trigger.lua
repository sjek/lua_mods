local core = require('openmw.core')
local camera = require('openmw.camera')
local util = require('openmw.util')
local self = require('openmw.self')

local function onKeyPress(key)
  if key.symbol == 'b' then
        local cenvec = camera.viewportToWorldVector(util.vector2(0.5,0.5)):normalize()
        core.sendGlobalEvent("sjek_throw_fireball", { centervect = cenvec, player = self } )
  end 
end

return { engineHandlers = { onKeyPress = onKeyPress } } 