## as title says

### this is done simply in onupdate handler

```lua
local core = require('openmw.core')
local world = require('openmw.world')
local time = require('openmw_aux.time')
local util = require('openmw.util')
local types = require('openmw.types')

local function onUpdate()

            local pl = world.activeActors -- this is for defining in what cell(s) the change is wanted
             
              if caster == nil then
                    local craft = {
                    id = "something",
                    model = "meshes\\m\\Misc_Potion_Quality_01.nif",
                    name = "caster"
                    }
          
                  local craftcaster = types.Activator.createRecordDraft(craft)
                  local ca = world.createRecord(craftcaster)  
                  caster = world.createObject( ca.id , #pl )   -- object is created for every actor in a stack
               
              end
              
             for m ,_ in pairs(pl) do
                
                  local one = caster:split(1)  -- stack is split to singulars 
                
                  time.runRepeatedly(function()
                    one:teleport( pl[m].cell , util.transform.move(0,0,200):apply(pl[m].position))
                  end, 0.1* time.second)
                
             end    
end


return { 
      engineHandlers = { onUpdate = onUpdate }
      } 

```

> here's a problem of repetitive error in console tho

```
[15:35:25.360 E] Global[spell mages/target pos.lua] onUpdate failed. Lua error: Can't remove 1 of 0 items
[15:35:25.360 E] stack traceback:
[15:35:25.360 E]        [C]: in function 'split'
[15:35:25.360 E]        [string "spell mages/target pos.lua"]:17: in function <[string "spell mages/target pos.lua"]:7>
```