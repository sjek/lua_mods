## as said

### Triggers 

#### keypress

```lua
local core = require('openmw.core')
local self = require('openmw.self')


local function onKeyPress(key)
           if key.symbol == 'v' then
              local se = self
                core.sendGlobalEvent( "eventname" , { player = se } )
           end
end

return { engineHandlers = { onKeyPress = onKeyPress } }
```


### things to add

#### ordinators

```lua
local world = require('openmw.world')
local types = require('openmw.types')
local core = require('openmw.core')

local function funcname(at)
    local posi = at.player.position
    local skel = world.createObject( "ordinator_high fane" , 1 )
    skel:teleport(at.player.cell , posi , { onGround = false } )
 end
 
 return {
     eventHandlers = { eventname = funcname }
 }
```