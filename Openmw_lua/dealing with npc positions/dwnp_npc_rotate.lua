local util = require('openmw.util')
local types = require('openmw.types')
local core = require('openmw.core')
local world = require('openmw.world')

return {
    engineHandlers = {
        onUpdate = function()
           local acts = world.activeActors
           for i, a in pairs(acts) do
            if acts[i].type ~= types.Player then
                acts[i]:teleport( acts[i].cell , acts[i].position, acts[i].rotation * util.transform.rotateZ(0.01) )
            end
           end
        end
    }
}