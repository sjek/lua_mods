local ai = require('openmw.interfaces').AI
local nearby = require('openmw.nearby')

function onActive()
  
  local acts = nearby.actors  -- this needs to be defined here
  for i, a in pairs(acts) do
    ai.startPackage({ type = 'Combat' , target = acts[i] })
  end
end

return { engineHandlers = { onActive = onActive } }
