
#### PLAYER script for sending the event

> This is simple event triggered by keypress. 
> The nearby containres are needed for fetching and player self for moving to it's inventory.
> Local player script is also needed for using nearby in this case

```lua
local nearby = require('openmw.nearby')
local core = require('openmw.core')
local self = require('openmw.self')
local types = require('openmw.types')


local function onKeyPress(key)
           if key.symbol == 'v' then
              local con = nearby.containers 
              local se = self
              for i, _ in pairs(con) do
                core.sendGlobalEvent( "fetch" , { cont = con[i], player = se } )
              end
           end
end

return { engineHandlers = { onKeyPress = onKeyPress } }
```
---

#### Resolving and fetching in GLOBAL script

> This script resolves the containers. 
> (ie. uses the levelled lists rasolving their content like in opening the container)
> And then gets the all ingredients from said containers. 

> In second step: The ingredient table is iterated and everything moved into player's inventory

```lua
local world = require('openmw.world')
local types = require('openmw.types')
local core = require('openmw.core')
local util = require('openmw.util')

local function functionname(so)
   types.Container.content(so.cont):resolve() 
        -- if not resolved, levelled lists aren't used
   local ingreds = types.Container.content(so.cont):getAll(types.Ingredient)
   
   -- core.sound.playSound3d("bell1", so.cont) 
        -- optional, plays a sound from container(s) position
   
   for a, _ in pairs(ingreds) do
      ing[a]:moveInto(types.Actor.inventory(so.player))
   end
end

return {
    eventHandlers = { fetch = functionname }
}
```