

```lua
local types = require('openmw.types')
local core = require('openmw.core')
local world = require('openmw.world')

return {
    engineHandlers = {
        onUpdate = function()
           local acts = world.activeActors
           for i, a in pairs(acts) do
            if acts[i].type ~= types.Player then
                         
              local pot = world.createObject( "p_invisibility_e" , 1 )
             
              if types.Actor.inventory(acts[i]):find( "p_invisibility_e" ) == nil then
                pot:moveInto(types.Actor.inventory(acts[i]))
              end
             
              if types.Actor.activeEffects(acts[i]):getEffect("Invisibility") == nil then
                core.sendGlobalEvent('UseItem', {object = pot, actor = acts[i]})
              end
              
            end
          end
        end
    }
}

```