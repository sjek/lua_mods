local camera = require("openmw.camera")
local nearby = require("openmw.nearby")
local util = require("openmw.util")
local ui = require('openmw.ui')

local function onKeyPress(key)
    if key.symbol == 'c' then
      
        pitch = -(camera.getPitch() + camera.getExtraPitch())
        yaw = (camera.getYaw() + camera.getExtraYaw())      -- these are from https://www.nexusmods.com/morrowind/mods/52202
        
        local xzLen = math.cos(pitch)
        local bor = util.vector3(
          xzLen * math.sin(yaw), -- x
          xzLen * math.cos(yaw), -- y
          math.sin(pitch)        -- z     -- and this angle calculus is from zackhasacat in discord
         ):normalize()        

      local pos = camera.getPosition()
      local rayC = nearby.castRenderingRay(pos, pos + bor * 20000)
      ui.showMessage( rayC.hitObject.recordId )
      
    end
end

return { engineHandlers = { onKeyPress = onKeyPress } }