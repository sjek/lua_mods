## start
 
- the caster is created on the fly with script made in CS attached to it.
- target is created in openmw-cs and both are currently activators with potion models

### first of to get the target position

```lua
local camera = require("openmw.camera")
local nearby = require("openmw.nearby")
local util = require("openmw.util")
local ui = require('openmw.ui')
local core = require('openmw.core')
local self = require('openmw.self')
local time = require('openmw_aux.time')

local function onKeyPress(key)
            if key.symbol == 'v' then
              
              pitch = -(camera.getPitch() + camera.getExtraPitch())
              yaw = (camera.getYaw() + camera.getExtraYaw())     -- these are from https://www.nexusmods.com/morrowind/mods/52202
        
              local xzLen = math.cos(pitch)
              local bor = util.vector3(
                xzLen * math.sin(yaw), -- x
                xzLen * math.cos(yaw), -- y
                math.sin(pitch)        -- z  -- this angle calculus is from zackhasacat in discord
                ):normalize()        

               local pos = camera.getPosition()
               local rayC = nearby.castRenderingRay(pos, pos + bor * 20000)
               

                  local p = rayC.hitPos 
                  local ce = self
              
                  core.sendGlobalEvent( "tele" ,{ player = ce, castrayhitpos = p })

            end
end


return { engineHandlers = { onKeyPress = onKeyPress } }
```

### then the script handling "casting" the spell via caster object and which should remove the objects after use


```lua
local core = require('openmw.core')
local world = require('openmw.world')
local types = require('openmw.types')
local time = require('openmw_aux.time')
local util = require('openmw.util')
local async = require('openmw.async')
local calendar = require('openmw_aux.calendar')


local function onPlayerAdded()
    
    local craft = {
          id = "something",
          model = "meshes\\m\\Misc_Potion_Quality_01.nif",
          mwscript = "aa_projectiles",
          name = "caster"
          }
          
    local craftcaster = types.Activator.createRecordDraft(craft)
    local ca = world.createRecord(craftcaster)  
    caster = world.createObject( ca.id , 1 )
    
end


local function onUpdate()
         local wo = world.cells
          for a, _ in pairs(wo) do
            local pl = wo[a]:getAll(types.Player) -- this is for defining in what cell(s) the change is wanted
             for m ,_ in pairs(pl) do
              if wo[a]:isInSameSpace(pl[1]) then  -- in same space as wanted object
                caster:teleport( wo[a] , util.transform.move(0,0,200):apply(pl[1].position))
                    -- this will move the wanted item along with the ( player in this case ) 
              end
             end
          end
end


local function start(ind)
   --print("11 event is fired at  ", calendar.gameTime() )
  
   target = world.createObject( "aa_pos" , 1)
      --print("22 target is created in  ", calendar.gameTime() )

   target:teleport( ind.player.cell , ind.castrayhitpos )
      --print("33 target is moved in  ", calendar.gameTime() )
      
          world.mwscript.getLocalScript(caster).variables.a = 1
          --print("55 cast is made in  ", calendar.gameTime() )
          -- casting the spell via wmscript      

      
       if target then
         async:newUnsavableSimulationTimer( 0.01 , function()  
            target:remove(1)    -- removing the target  
            --print("66 target is removed in  ", calendar.gameTime())
            --caster:remove(1)
         end)
       end                         
       
end

 
return { 
      eventHandlers = { tele = start },
      engineHandlers = { onPlayerAdded = onPlayerAdded, onUpdate = onUpdate }
      } 
```


### this is on projectilebox script in openmw-cs

```
Begin aa_projectiles

short a

if ( a == 1 ) 
	cast fireball aa_pos
	set a to 3
endif

End aa_projectiles

```
