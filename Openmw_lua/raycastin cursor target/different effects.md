## adding things

### modification to the view hit position ( rct_palyer.lua )

```lua
      ce = self
      p = rayC.hitPos
      
      core.sendGlobalEvent( "sum_at" ,{ sump = p, c = ce })
```

#### different choises

##### flame attronatch

```lua
local world = require('openmw.world')
local core = require('openmw.core')


local function func_sump(att)
    world.createObject("atronach_flame", 1 ):teleport( att.c.cell , att.sump )
end

return {
    eventHandlers = { sum_at = func_sump }
}
```

### 2.0 modification to the view hit position ( rct_palyer.lua )

```lua
              local p = rayC.hitPos 
              local ce = self
              core.sendGlobalEvent( "cast" ,{ sel = ce, pos = p })
```

#### teleporting two CS created objects and casting fireball from one to another

- thanks to advice on discord, know to use async:newUnsavableSimulationTimer for skipping frames

```lua
local world = require('openmw.world')
local util = require('openmw.util')
local async = require('openmw.async')


local function remo(atti)

      -- creating the objects 
  local aa = world.createObject( "aa_pos" , 1)
  local proj = world.createObject("aa_projectilebox", 1 )
  
  proj:teleport( atti.sel.cell , util.transform.move(0,0,200):apply(atti.sel.position))
      -- teleports caster over player head
  aa:teleport( atti.sel.cell , atti.pos )
      -- teleports target to castray position 
      
     async:newUnsavableSimulationTimer( 0.1 , function()  
        world.mwscript.getLocalScript(proj).variables.a = 1    
        -- casting the spell via wmscript          
     end)   
   async:newUnsavableSimulationTimer( 0.2 , function()  
      aa:remove(1)    -- removing the objects
      proj:remove(1)
   end)         
end

return { eventHandlers = { cast = remo} 
}

```

### Then placing following item above player head, This one is simpler and doesn't require castray

#### in PLAYER script simply send event on keypress

```lua
local core = require('openmw.core')

 local function onKeyPress(key)
            if key.symbol == 'v' then
              core.sendGlobalEvent( "tele" )
            end
        end

return { engineHandlers = { onKeyPress = onKeyPress } }
```

#### Then in the GLOBAL script create the item via event 
- and use onupdate with teleport transform vector to apply the movement to the said item

```lua
local core = require('openmw.core')
local world = require('openmw.world')
local types = require('openmw.types')
local util = require('openmw.util')


local function onUpdate(dt)
     if proj ~= nil then   -- this needs to be here to prevent console spam about non existent item on teleport
        local wo = world.cells
          for a, _ in pairs(wo) do
            local pl = wo[a]:getAll(types.Player) -- this is for defining in what cell(s) the change is wanted
             for m ,_ in pairs(pl) do
              if wo[a]:isInSameSpace(pl[1]) then  -- in same space as wanted object
                proj:teleport( wo[a] , util.transform.move(0,0,200):apply(pl[1].position))
                    -- this will move the wanted item along with the ( player in this case ) 
              end
             end
          end
      end   
end


local function start()
   proj = world.createObject("aa_projectilebox", 1 )
        -- this wil create the object. 
        -- with just this the object is added without removing the previous
end

 
return { 
      engineHandlers = { onUpdate = onUpdate }, 
      eventHandlers = { tele = start }
      } 
```
