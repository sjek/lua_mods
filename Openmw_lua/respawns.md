on container

```lua
local self = require('openmw.self')
local core = require('openmw.core')

    local items = {}
    local counts = {}
    local times
    
 
local function onActivated() 

      if next(items) == nil then 
        if times == nil then
          print("activated")
          core.sendGlobalEvent("sjek_resolve", { target = self , itemtable = items, counts = counts } )
          times = 1
        end
    end        
end

return { engineHandlers = { 
            onActivated = onActivated 
            }} 
```
on global:
```lua
local types = require('openmw.types')
local world = require('openmw.world')
local time = require('openmw_aux.time')
local calendar = require('openmw_aux.calendar')

local function sjek_resolve(var1)
  types.Container.content(var1.target):resolve()
                    
            local contain = types.Container.content(var1.target):getAll()
  
              for i,_ in pairs(contain) do
                table.insert(var1.itemtable, contain[i].recordId) 
                table.insert(var1.counts, types.Container.content(var1.target):countOf(contain[i].recordId))
              end

                items = var1.itemtable
                counts = var1.counts
                cont = var1.target

        time.runRepeatedly(function()
          --print(tostring(calendar.gameTime()))
          for i = 1, #var1.itemtable do
            if types.Container.content(var1.target):find(var1.itemtable[i]) == nil then
              print(var1.itemtable[i],  "count",  tostring(var1.counts[i]))
              world.createObject(var1.itemtable[i],var1.counts[i]):moveInto(var1.target)
            end  
          end
        end,1*time.day,{type = time.GameTime})
  end

local function onSave()
  return { 
    itemsave = items,
    countsave = counts,
    consave = cont   
  }
end

local function onLoad(data)
        time.runRepeatedly(function()
          for i = 1, #data.itemsave do
            if types.Container.content(data.consave):find(data.itemsave[i]) == nil then
              print(data.itemsave[i],  "count",  tostring(data.countsave[i]))
              world.createObject(data.itemsave[i],data.countsave[i]):moveInto(data.consave)
            end  
          end
        end,1*time.day,{type = time.GameTime})
end

return {
    engineHandlers = {
        onSave = onSave,
        onLoad = onLoad },
    eventHandlers = { sjek_resolve = sjek_resolve }}
```